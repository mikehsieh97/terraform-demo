# GitOps Demo with Terraform

### Infrastructure as Code

Example using Terraform to manage using good GitLab Flow.

**Must follow good GitLab flow:**
1. Create an Issue -- This is the problem statement or user story.
2. Create a Merge Request -- This is the fix or solution that will resolve the Issue, or at least MVC.
3. Approve MR, and merge to `master`.

### Documentation Links
1. https://docs.gitlab.com/ee/user/infrastructure/
2. https://docs.gitlab.com/ee/administration/terraform_state.html#terraform-state-administration-alpha
